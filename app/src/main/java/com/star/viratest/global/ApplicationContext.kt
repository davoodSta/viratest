package com.star.viratest.global

import android.content.Context

class ApplicationContext private constructor() {
    private var context: Context? = null
    fun init(context: Context?) {
        if (this.context == null) {
            this.context = context
        }
    }

    companion object {
        fun get(): Context? {
            return instance!!.context
        }

        var instance: ApplicationContext? = null
            get() = if (field == null) ApplicationContext().also {
                field = it
            } else field
            private set
    }
}