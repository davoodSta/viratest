package com.star.viratest.global

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ViraTestApplication: Application() {


    override fun onCreate() {
        super.onCreate()

        ApplicationContext.instance?.init(this)

    }

}