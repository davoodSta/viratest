package com.star.viratest.view.footballresult

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.star.viratest.R
import com.star.viratest.global.ApplicationContext
import com.star.viratest.model.dataClass.Game
import com.star.viratest.model.dataClass.GameStatus
import kotlinx.android.synthetic.main.row_game.view.*

class Adapter_GameResult
    (private val listGame: ArrayList<Game>):
    RecyclerView.Adapter<Adapter_GameResult.GameResultViewHolder>() {

    class GameResultViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(game: Game) {
            itemView.tvHostTeamName.text = game.hostTeamName
            itemView.tvGuestTeamNameName.text = game.guestTeamName
            when (game.gameStatus)
            {
                GameStatus.CANCELED -> {
                    itemView.tvGameResult.visibility = View.GONE
                    itemView.tvGameStatus.text = ApplicationContext.get()?.getString(R.string.canceled)
                    itemView.tvGameStartTime.visibility = View.GONE
                    itemView.tvGameSpendedTime.visibility = View.GONE
                }
                GameStatus.LIVE -> {
                    itemView.tvGameResult.visibility = View.VISIBLE
                    itemView.tvGameResult.text = game.gameResult
                    itemView.tvGameStatus.text = ApplicationContext.get()?.getString(R.string.live)
                    itemView.tvGameStartTime.visibility = View.GONE
                    itemView.tvGameSpendedTime.visibility = View.VISIBLE
                    itemView.tvGameSpendedTime.text = game.gameSpendedTime
                }

                GameStatus.NOT_STARTED -> {
                    itemView.tvGameStatus.visibility = View.GONE
                    itemView.tvGameResult.visibility = View.GONE
                    itemView.tvGameStartTime.visibility = View.VISIBLE
                    itemView.tvGameStartTime.text = game.gameTime
                    itemView.tvGameSpendedTime.visibility = View.GONE
                }
            }

            ApplicationContext.get()?.let {
                Glide.with(it)
                    .load(game.hostTeamLogo)
                    .apply(
                        RequestOptions().override(70, 70)
                            .placeholder(R.drawable.no_image_placeholder)
                            .error(R.drawable.error_image)
                    )
                    .into(itemView.ivHostTeamLogo)
            }

            ApplicationContext.get()?.let {
                Glide.with(it)
                    .load(game.guestTeamLogo)
                    .apply(
                        RequestOptions().override(70, 70)
                            .placeholder(R.drawable.no_image_placeholder)
                            .error(R.drawable.error_image)
                    )
                    .into(itemView.ivGuestTeamLogo)
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GameResultViewHolder {
        return GameResultViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.row_game, parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return listGame.size
    }

    override fun onBindViewHolder(holder: GameResultViewHolder, position: Int) {
        holder.bind(listGame.get(position))
    }

}