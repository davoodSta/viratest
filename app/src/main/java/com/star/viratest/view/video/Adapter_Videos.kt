package com.star.viratest.view.video

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.star.viratest.R
import com.star.viratest.global.ApplicationContext
import com.star.viratest.model.dataClass.VideoInfo
import kotlinx.android.synthetic.main.row_video.view.*

class Adapter_Videos
    (private val listVideos : ArrayList<VideoInfo>) :
    RecyclerView.Adapter<Adapter_Videos.VideoViewHolder>() {

    class VideoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(videoInfo: VideoInfo) {
            itemView.tvTime.text = videoInfo.duration.toString()
            ApplicationContext.get()?.let {
                Glide.with(it)
                    .load(videoInfo.getVideoThumbnail(videoInfo.videoPath))
                    .apply(RequestOptions().override(75, 75).placeholder(R.drawable.no_image_placeholder).error(R.drawable.error_image))
                    .into(itemView.ivImage)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideoViewHolder {
        return VideoViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.row_video, parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return listVideos.size
    }

    override fun onBindViewHolder(holder: VideoViewHolder, position: Int) {
        holder.bind(listVideos.get(position))
    }

}