package com.star.viratest.view.footballresult

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.star.viratest.R
import com.star.viratest.model.callbackInterfaces.ICallBackItemClick
import com.star.viratest.model.dataClass.League
import kotlinx.android.synthetic.main.row_league.view.*

class Adapter_Leagues (private val listItems : ArrayList<League>,private val m_listener: ICallBackItemClick) :
    RecyclerView.Adapter<Adapter_Leagues.LeagueItemsViewHolder>() {

    class LeagueItemsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(league: League) {
            itemView.tvLeagueName.text = league.leagueName
            Glide.with(itemView.ivLeagueIcon.context)
                .load(league.leagueIcon)
                .apply(
                    RequestOptions().override(90, 90).placeholder(R.drawable.no_image_placeholder).error(
                        R.drawable.error_image))
                .into(itemView.ivLeagueIcon)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LeagueItemsViewHolder {
        return LeagueItemsViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.row_league, parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return listItems.size
    }

    override fun onBindViewHolder(holder: LeagueItemsViewHolder, position: Int) {
        holder.bind(listItems.get(position))

        holder.itemView.setOnClickListener(View.OnClickListener { v ->
            if (position == 0)
            {
                m_listener.onItemClicked()
            }
        })
    }

}