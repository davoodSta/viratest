package com.star.viratest.view.footballresult

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.tabs.TabLayout
import com.star.viratest.R
import com.star.viratest.helper.TabLayoutHelper
import com.star.viratest.helper.Utils
import com.star.viratest.model.callbackInterfaces.ICallBackItemClick
import com.star.viratest.model.dataClass.League
import com.star.viratest.model.callbackInterfaces.ICallBackTabSelect
import com.star.viratest.view.footballresult.fragmentGames.Fragment_DayOfterTomorrow
import com.star.viratest.view.footballresult.fragmentGames.Fragment_Today
import com.star.viratest.view.footballresult.fragmentGames.Fragment_Tomorrow
import com.star.viratest.view.footballresult.fragmentGames.Fragment_Yesterday
import com.star.viratest.view.video.Activity_Videos
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_foot_ball.*


@AndroidEntryPoint
class Activity_FootBall : AppCompatActivity() , ICallBackTabSelect , ICallBackItemClick{

    private lateinit var uptabLayoutHelper: TabLayoutHelper

    private lateinit var bottomtabLayoutHelper: TabLayoutHelper

    private lateinit var adapterLeagues: Adapter_Leagues

    private lateinit var listLeagues: ArrayList<League>

    private lateinit var league: League

    private lateinit var fragment: Fragment

    private lateinit var listIcons: ArrayList<Int>

    private var TAG = "Activity_Videos"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_foot_ball)

        populateLeagueItems()

        setupUI()

    }

    private fun getListIcons(): ArrayList<Int>
    {
        listIcons = arrayListOf()
        listIcons.add(R.drawable.ic_home)
        listIcons.add(R.drawable.ic_game)
        listIcons.add(R.drawable.ic_add)
        listIcons.add(R.drawable.ic_wallet)
        listIcons.add(R.drawable.ic_profile)

        return listIcons
    }

    private fun setupUI()
    {
        uptabLayoutHelper = TabLayoutHelper(tlDates, this)

        bottomtabLayoutHelper = TabLayoutHelper(tlMenu,getListIcons() , this)

        initializeRecyclerView()

        initializeFragment()
    }

    private fun initializeRecyclerView()
    {
        rvLeagues.layoutManager = LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false)

        adapterLeagues = Adapter_Leagues(listLeagues, this)

        rvLeagues.adapter = adapterLeagues
    }

    private fun initializeFragment()
    {
        fragment =
            Fragment_Yesterday()
        replaceFragment(fragment)
    }

    private fun populateLeagueItems()
    {
        listLeagues = arrayListOf()
        league = League()
        league.leagueName = getString(R.string.select_date)
        league.leagueIcon = getResources().getDrawable(getResources()
            .getIdentifier("ic_calendar", "drawable", getPackageName()))
        listLeagues.add(league)

        league = League()
        league.leagueName = getString(R.string.favorites)
        league.leagueIcon = getResources().getDrawable(getResources()
            .getIdentifier("ic_heart", "drawable", getPackageName()))
        listLeagues.add(league)

        league = League()
        league.leagueName = getString(R.string.premier_league)
        league.leagueIcon = getResources().getDrawable(getResources()
            .getIdentifier("premier_league", "drawable", getPackageName()))
        listLeagues.add(league)

        league = League()
        league.leagueName = getString(R.string.america_league)
        league.leagueIcon = getResources().getDrawable(getResources()
            .getIdentifier("america_mls", "drawable", getPackageName()))
        listLeagues.add(league)

        league = League()
        league.leagueName = getString(R.string.spania_league)
        league.leagueIcon = getResources().getDrawable(getResources()
            .getIdentifier("laliga", "drawable", getPackageName()))
        listLeagues.add(league)

    }

    override fun onTabSelected(tab: TabLayout.Tab?) {

        when (tab?.position)
        {
            0 -> {
                tvTitle.text = getString(R.string.yesterday_games)
                fragment =
                    Fragment_Yesterday()
            }

            1 -> {
                tvTitle.text = getString(R.string.today_games)
                fragment =
                    Fragment_Today()
            }

            2 -> {
                tvTitle.text = getString(R.string.tomorrow_games)
                fragment =
                    Fragment_Tomorrow()
            }

            3 -> {
                tvTitle.text = "بازی های " + Utils.getDateToPersian()
                fragment =
                    Fragment_DayOfterTomorrow()
            }

        }

        replaceFragment(fragment)

    }

    private fun replaceFragment(fragment: Fragment)
    {
        val fm: FragmentManager = supportFragmentManager
        val ft: FragmentTransaction = fm.beginTransaction()
        ft.replace(R.id.flResults, fragment)
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        ft.commit()
    }

    override fun onItemClicked() {
        Utils.startActivity(this,Activity_Videos::class.java)
    }

}