package com.star.viratest.view.footballresult.fragmentGames

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.star.viratest.R
import com.star.viratest.model.dataClass.Game
import com.star.viratest.model.dataClass.GameStatus
import com.star.viratest.view.footballresult.Adapter_GameResult
import kotlinx.android.synthetic.main.fragment_yesterday.view.*

class Fragment_Yesterday : Fragment() {

    private lateinit var adapterGameresult: Adapter_GameResult

    private lateinit var listGame: ArrayList<Game>
    private lateinit var game: Game

    internal var root: View? = null

    @Override
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    @Override
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        root =  inflater.inflate(R.layout.fragment_yesterday,container,false)

        setupUI()

        return root
    }

    private fun setupUI()
    {
        populateGames()
        root!!.rvYesterdayGameResult.layoutManager = LinearLayoutManager(this.context,
            LinearLayoutManager.VERTICAL,false)
        adapterGameresult =
            Adapter_GameResult(listGame)
        root!!.rvYesterdayGameResult.adapter = adapterGameresult
    }

    private fun populateGames()
    {

        listGame = arrayListOf()

        game = Game()
        game.hostTeamName = getString(R.string.manchester_united)
        game.hostTeamLogo = getResources().getDrawable(getResources()
            .getIdentifier("ic_manchester_united", "drawable", activity?.getPackageName()))
        game.guestTeamName = getString(R.string.manchester_city)
        game.guestTeamLogo = getResources().getDrawable(getResources()
            .getIdentifier("ic_manchester_city", "drawable", activity?.getPackageName()))
        game.gameStatus = GameStatus.LIVE
        game.gameResult = getString(R.string.sample_result)
        game.gameSpendedTime = getString(R.string.sample_game_spended_time)
        listGame.add(game)

        game = Game()
        game.hostTeamName = getString(R.string.chelsea)
        game.hostTeamLogo = getResources().getDrawable(getResources()
            .getIdentifier("ic_chelsea", "drawable", activity?.getPackageName()))
        game.guestTeamName = getString(R.string.bornly)
        game.guestTeamLogo = getResources().getDrawable(getResources()
            .getIdentifier("ic_bornly", "drawable", activity?.getPackageName()))
        game.gameStatus = GameStatus.CANCELED
        game.gameResult = getString(R.string.sample_result)
        game.gameSpendedTime = getString(R.string.sample_game_spended_time)
        game.gameTime = ""
        listGame.add(game)

        game = Game()
        game.hostTeamName = getString(R.string.astonvilla)
        game.hostTeamLogo = getResources().getDrawable(getResources()
            .getIdentifier("ic_astonvilla", "drawable", activity?.getPackageName()))
        game.guestTeamName = getString(R.string.brighton)
        game.guestTeamLogo = getResources().getDrawable(getResources()
            .getIdentifier("ic_brighton", "drawable", activity?.getPackageName()))
        game.gameStatus = GameStatus.NOT_STARTED
        game.gameResult = getString(R.string.sample_result)
        game.gameSpendedTime = getString(R.string.sample_game_spended_time)
        game.gameTime = getString(R.string.sample_time)
        listGame.add(game)

        game = Game()
        game.hostTeamName = getString(R.string.newcastle_united)
        game.hostTeamLogo = getResources().getDrawable(getResources()
            .getIdentifier("ic_newcastle_united", "drawable", activity?.getPackageName()))
        game.guestTeamName = getString(R.string.leicster_city)
        game.guestTeamLogo = getResources().getDrawable(getResources()
            .getIdentifier("ic_leicester_city", "drawable", activity?.getPackageName()))
        game.gameStatus = GameStatus.NOT_STARTED
        game.gameResult = getString(R.string.sample_result)
        game.gameSpendedTime = getString(R.string.sample_game_spended_time)
        game.gameTime = getString(R.string.sample_time)
        listGame.add(game)

    }

}