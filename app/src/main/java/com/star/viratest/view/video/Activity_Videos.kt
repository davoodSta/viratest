package com.star.viratest.view.video

import android.Manifest
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.GridLayoutManager
import com.star.viratest.R
import com.star.viratest.helper.PermissionHelper
import com.star.viratest.helper.Utils
import com.star.viratest.model.dataClass.VideoInfo
import com.star.viratest.model.callbackInterfaces.ICallBackPermission
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_videos.*

@AndroidEntryPoint
class Activity_Videos : AppCompatActivity(), ICallBackPermission {

    private lateinit var adapter: Adapter_Videos

    private lateinit var permissionHelper: PermissionHelper

    private lateinit var listVideos: ArrayList<VideoInfo>

    val REQUEST_CODE_READ_EXTERNAL_STORAGE = 101

    val REQUEST_CODE_WRITE_EXTERNAL_STORAGE = 201

    private var TAG = "Activity_Videos"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_videos)

        checkPermission()
    }

    private fun checkPermission()
    {
        permissionHelper = PermissionHelper(this,Manifest.permission.READ_EXTERNAL_STORAGE,REQUEST_CODE_READ_EXTERNAL_STORAGE,this)
    }

    private fun setupUI() {
        rvVideos.layoutManager = GridLayoutManager(this, 2)
        adapter = Adapter_Videos(listVideos)
        rvVideos.adapter = adapter
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when(requestCode)
        {
            REQUEST_CODE_READ_EXTERNAL_STORAGE -> {

                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i( TAG, "Permission Granted")
                    listVideos = Utils.getVideos()
                    setupUI()
                }
                else
                {
                    Log.i( TAG, "Permission Denied")
                    Utils.printToast("Permission Denied")
                    checkPermission()
                }

            }

        }

    }

    override fun onPermissionGranted() {
        listVideos = Utils.getVideos()
        setupUI()
    }
}