package com.star.viratest.helper;

import android.app.Activity;

import com.google.android.material.tabs.TabLayout;
import com.star.viratest.R;
import com.star.viratest.global.ApplicationContext;
import com.star.viratest.model.callbackInterfaces.ICallBackTabSelect;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class TabLayoutHelper implements TabLayout.OnTabSelectedListener
{

    ICallBackTabSelect m_listener;

    @Inject
    public TabLayoutHelper(TabLayout tabLayout ,ICallBackTabSelect listener)
    {

        tabLayout.setTabMode(TabLayout.MODE_FIXED);

        TabLayout.Tab yesterdayTab = tabLayout.newTab();
        yesterdayTab.setText(ApplicationContext.Companion.get().getString(R.string.yesterday));
        tabLayout.addTab(yesterdayTab);

        TabLayout.Tab todayTab = tabLayout.newTab();
        todayTab.setText(ApplicationContext.Companion.get().getString(R.string.today));
        tabLayout.addTab(todayTab);

        TabLayout.Tab tomorrowTab = tabLayout.newTab();
        tomorrowTab.setText(ApplicationContext.Companion.get().getString(R.string.tomorrow));
        tabLayout.addTab(tomorrowTab);

        TabLayout.Tab dayAfterTomorrowTab = tabLayout.newTab();
        dayAfterTomorrowTab.setText(Utils.getDateToPersian());
        tabLayout.addTab(dayAfterTomorrowTab);

        tabLayout.addOnTabSelectedListener(this);

        m_listener = listener;
    }

    public TabLayoutHelper(TabLayout tabLayout, ArrayList<Integer> icons, ICallBackTabSelect listener)
    {

        tabLayout.setTabMode(TabLayout.MODE_FIXED);

        TabLayout.Tab homeTab = tabLayout.newTab();
        tabLayout.addTab(homeTab);

        TabLayout.Tab gameTab = tabLayout.newTab();
        tabLayout.addTab(gameTab);

        TabLayout.Tab addTab = tabLayout.newTab();
        tabLayout.addTab(addTab);

        TabLayout.Tab walletTab = tabLayout.newTab();
        tabLayout.addTab(walletTab);

        TabLayout.Tab profileTab = tabLayout.newTab();
        tabLayout.addTab(profileTab);

        tabLayout.getTabAt(0).setIcon(icons.get(0));
        tabLayout.getTabAt(1).setIcon(icons.get(1));
        tabLayout.getTabAt(2).setIcon(icons.get(2));
        tabLayout.getTabAt(3).setIcon(icons.get(3));
        tabLayout.getTabAt(4).setIcon(icons.get(4));

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        m_listener.onTabSelected(tab);
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}
