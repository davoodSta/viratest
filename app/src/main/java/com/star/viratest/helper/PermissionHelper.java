package com.star.viratest.helper;

import android.app.Activity;
import android.content.pm.PackageManager;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.star.viratest.R;
import com.star.viratest.global.ApplicationContext;
import com.star.viratest.model.callbackInterfaces.ICallBackPermission;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class PermissionHelper {

    @Inject
    public PermissionHelper(Activity activity, String permission, int requestCode, ICallBackPermission listener)
    {
        if (!isPermissionGranted(permission))
        {
            requestForPermission(activity, permission, requestCode);
        }
        else
        {
            listener.onPermissionGranted();
        }
    }

    private boolean isPermissionGranted(String permission) {
        int result = ContextCompat.checkSelfPermission(ApplicationContext.Companion.get(), permission);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    public void requestForPermission(Activity activity, String permission, int requestCode)
    {
        if (!isPermissionGranted(permission))
        ActivityCompat.requestPermissions(activity, new String[]{permission},requestCode);
        else Utils.printToast(ApplicationContext.Companion.get().getString(R.string.permission_already_granted));
    }
}
