package com.star.viratest.helper;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

import com.star.viratest.global.ApplicationContext;
import com.star.viratest.helper.Time.PersianDate;
import com.star.viratest.helper.Time.PersianDateFormat;
import com.star.viratest.model.dataClass.VideoInfo;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class Utils {

    @NotNull
    public static ArrayList<VideoInfo> getVideos() {

        Uri uri;
        Cursor cursor;
        int column_index_data, column_index_folder_name, column_id, thum;
        ArrayList<VideoInfo> videoInfos = new ArrayList<>();

        String absolutePathOfImage = null;
        uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;

        String[] projection = {MediaStore.MediaColumns.DATA, MediaStore.Video.Media.BUCKET_DISPLAY_NAME, MediaStore.Video.Media._ID, MediaStore.Video.Thumbnails.DATA};

        final String orderBy = MediaStore.Images.Media.DATE_TAKEN;
        cursor = ApplicationContext.Companion.get().getContentResolver().query(uri, projection, null, null, orderBy + " DESC");

        column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        column_index_folder_name = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.BUCKET_DISPLAY_NAME);
        column_id = cursor.getColumnIndexOrThrow(MediaStore.Video.Media._ID);
        thum = cursor.getColumnIndexOrThrow(MediaStore.Video.Thumbnails.DATA);

        while (cursor.moveToNext()) {
            absolutePathOfImage = cursor.getString(column_index_data);
            Log.i("Column", absolutePathOfImage);
            Log.i("Folder", cursor.getString(column_index_folder_name));
            Log.i("column_id", cursor.getString(column_id));
            Log.i("thum", cursor.getString(thum));

            VideoInfo obj_videoInfo = new VideoInfo();
            obj_videoInfo.setVideoPath(absolutePathOfImage);
            obj_videoInfo.setVideoId(column_id);

            videoInfos.add(obj_videoInfo);

        }

        return videoInfos;
    }

    // MESSAGE TO USER
    public static void printToast(String message)
    {
        Toast.makeText(ApplicationContext.Companion.get(),message,Toast.LENGTH_SHORT).show();
    }

    //GET DATE AND TIME IN PERSIAN
    public static String getDateToPersian() {
        PersianDate pDate = new PersianDate();
        new PersianDateFormat("yyyy/mm/dd").format(pDate);
        return  pDate.dayName() + " "
                + (pDate.getShDay()) + " "
                + pDate.monthName(pDate.getShMonth());
    }

    // START ACTIVITY
    public static void startActivity(Activity thisActivity, Class<?> secondActivity)
    {
        Intent intent = new Intent(thisActivity, secondActivity);
        thisActivity.startActivity(intent);
    }


}
