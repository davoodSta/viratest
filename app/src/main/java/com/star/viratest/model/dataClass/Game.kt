package com.star.viratest.model.dataClass

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import java.io.Serializable

class Game : Serializable, Cloneable {

    var id: Int = 0

    var hostTeamName: String? = null

    var hostTeamLogo: Drawable? = null

    var guestTeamName: String? = null

    var guestTeamLogo: Drawable? = null

    var gameTime: String? = null

    var gameStatus: GameStatus? = null

    var gameResult: String? = null

    var gameSpendedTime: String? = null
}