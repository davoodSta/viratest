package com.star.viratest.model.callbackInterfaces;

public interface ICallBackPermission {

    void onPermissionGranted();

}
