package com.star.viratest.model.dataClass

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.ThumbnailUtils
import android.provider.MediaStore
import com.star.viratest.global.ApplicationContext
import java.io.Serializable

class VideoInfo : Serializable, Cloneable {
    var videoId: Long = 0
    var videoName = ""
    var videoPath: String? = null
    var duration: Long = 0
    var videoThumbnail : Bitmap? = null

    fun getVideoThumbnail(videoPath: String?): Bitmap
    {
        var options= BitmapFactory.Options()
        options.inSampleSize = 1;

        //var curThumb = MediaStore.Video.Thumbnails.getThumbnail(crThumb, videoId, MediaStore.Video.Thumbnails.MICRO_KIND, options)

        var curThumb = ThumbnailUtils.createVideoThumbnail(videoPath, MediaStore.Video.Thumbnails.MICRO_KIND);

        return curThumb
    }

}