package com.star.viratest.model.dataClass

enum class GameStatus {

    LIVE,
    CANCELED,
    NOT_STARTED
}