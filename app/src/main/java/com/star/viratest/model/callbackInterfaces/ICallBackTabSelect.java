package com.star.viratest.model.callbackInterfaces;

import com.google.android.material.tabs.TabLayout;

public interface ICallBackTabSelect {

    void onTabSelected(TabLayout.Tab tab);
}
