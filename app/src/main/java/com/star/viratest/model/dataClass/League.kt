package com.star.viratest.model.dataClass

import android.graphics.drawable.Drawable
import java.io.Serializable

class League : Serializable, Cloneable{

    var leagueName: String? = null
    var leagueIcon: Drawable? = null

}