package com.star.viratest.model.callbackInterfaces;

public interface ICallBackItemClick {

    void onItemClicked();

}
